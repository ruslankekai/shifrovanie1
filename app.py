from flask import Flask, render_template, request, url_for, redirect
from python import HILL, AFFINE, aes128, CEZAR
import os
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import base64

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///contact.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    intro = db.Column(db.String(300), nullable=False)
    text = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime(300), default=datetime.utcnow)

    def __repr__(self):
        return '<Article %r>' % self.id






@app.route('/')
def index():
    return render_template('index.html')


@app.errorhandler(404)
def pageNotFound(error):
    return render_template('page404.html')


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/posts')
def posts():
    articles = Article.query.order_by(Article.date.desc()).all()
    return render_template("posts.html", articles=articles)


@app.route('/posts/<int:id>')
def post_detail(id):
    article = Article.query.get(id)
    return render_template("post_detail.html", article=article)


@app.route('/posts/<int:id>/delete')
def post_delete(id):
    article = Article.query.get_or_404(id)

    try:
        db.session.delete(article)
        db.session.commit()
        return redirect("/posts")
    except:
        return "При удалении статьи произошла ошибка"


@app.route('/posts/<int:id>/update', methods=['POST', 'GET'])
def post_update(id):
    article = Article.query.get_or_404(id)
    if request.method == "POST":
        article.title = request.form['title']
        article.intro = request.form['intro']
        article.text = request.form['text']
        try:
            db.session.commit()
            return redirect("/posts")
        except:
            return "При обновлении статьи произошла ошибка"
    else:
        return render_template('post_update.html', article=article)


@app.route('/create-article', methods=['POST', 'GET'])
def create_article():
    if request.method == "POST":
        title = request.form['title']
        intro = request.form['intro']
        text = request.form['text']

        article = Article(title=title, intro=intro, text=text)
        try:
            db.session.add(article)
            db.session.commit()
            return redirect("/posts")
        except:
            return "При добавлении статьи произошла ошибка"
    else:
        return render_template('create-article.html')


################################################################
@app.route('/cezar')
def cezar():
    return render_template("cezar.html")


@app.route('/cezar_encryption', methods=['POST'])
def cezar_encryption():
    inputText = request.form['text']
    input_shift = int(request.form['shift'])
    inputText = CEZAR.cezar_crypt(inputText, input_shift, 'encrypt')
    inputText = ("".join(inputText))
    return render_template('cezar.html', text = inputText)


@app.route('/cezar_decryption', methods=['POST'])
def cezar_decryption():
    inputText = request.form['text1']
    input_shift = int(request.form['shift1'])
    inputText = CEZAR.cezar_crypt(inputText, input_shift, 'decrypt')
    inputText = ("".join(inputText))
    return render_template('cezar.html', text1 =inputText)


################################################################


################################################################
@app.route('/hill')
def hill():
    return render_template("hill.html")


@app.route('/hill_encryption', methods=['POST'])
def hill_encryption():
    inputText = request.form['text']
    inputKey = request.form['key']
    inputKey2 = None
    inputText = HILL.encryption_hill(inputText, inputKey2, 'encrypt', inputKey, inputKey2)
    inputText = ("".join(inputText))
    return render_template('hill.html', text = inputText, key = inputKey)


@app.route('/hill_decryption', methods=['POST'])
def hill_decryption():
    inputText = request.form['text1']
    inputKey1 = request.form['key1']
    inputKey2 = None
    inputText = HILL.encryption_hill(inputText, inputKey2, 'decrypt', inputKey1, inputKey2)
    inputText = ("".join(inputText))
    return render_template('hill.html', text1 =inputText)


@app.route('/hill_reccurent')
def hill_reccurent():
    return render_template("hill_reccurent.html")


@app.route('/hill_reccurent_encryption', methods=['POST'])
def hill_reccurent_encryption():
    inputText = request.form['text']
    inputKey1 = request.form['key1']
    inputKey2 = request.form['key2']
    inputText = HILL.encryption_hill(inputText, 1, 'encrypt', inputKey1, inputKey2)
    inputText = ("".join(inputText))
    return render_template('hill_reccurent.html', text = inputText, key = inputKey1, key2 = inputKey2)


@app.route('/hill_reccurent_decryption', methods=['POST'])
def hill_reccurent_decryption():
    inputText = request.form['text1']
    inputKey1 = request.form['key1']
    inputKey2 = request.form['key2']
    inputText = HILL.encryption_hill(inputText, 1, 'decrypt', inputKey1, inputKey2)
    inputText = ("".join(inputText))
    return render_template('hill_reccurent.html', text1 =inputText)
############################################################################


############################################################################
@app.route('/affine')
def affine():
    return render_template("affine.html")


@app.route('/affine_encryption', methods=['POST'])
def affine_encryption():
    inputText = request.form['text']
    input_a = int(request.form['a'])
    input_b = int(request.form['b'])
    a1, b1 = None, None
    inputText = AFFINE.encryption_affin(inputText, input_a, input_b, a1, b1, 'encrypt')
    inputText = ("".join(inputText))
    return render_template('affine.html', text = inputText)


@app.route('/affine_decryption', methods=['POST'])
def affine_decryption():
    inputText = request.form['text1']
    input_a = int(request.form['a'])
    input_b = int(request.form['b'])
    a1, b1 = None, None
    inputText =  AFFINE.encryption_affin(inputText, input_a, input_b, a1, b1, 'decrypt')
    inputText = ("".join(inputText))
    return render_template('affine.html', text1 =inputText)


@app.route('/affine_reccurent')
def affin_reccurent():
    return render_template("affine_reccurent.html")


@app.route('/affine_reccurent_encryption', methods=['POST'])
def affin_reccurent_encryption():
    inputText = request.form['text']
    input_a = int(request.form['a'])
    input_b = int(request.form['b'])
    input_a1 = int(request.form['a1'])
    input_b1 = int(request.form['b1'])
    inputText =  AFFINE.encryption_affin(inputText, input_a, input_b, input_a1, input_b1, 'encrypt')
    inputText = ("".join(inputText))
    return render_template('affine_reccurent.html', text = inputText)


@app.route('/affine_reccurent_decryption', methods=['POST'])
def affin_reccurent_decryption():
    inputText = request.form['text1']
    input_a = int(request.form['a'])
    input_b = int(request.form['b'])
    input_a1 = int(request.form['a1'])
    input_b1 = int(request.form['b1'])
    inputText =  AFFINE.encryption_affin(inputText, input_a, input_b, input_a1, input_b1, 'decrypt')
    inputText = ("".join(inputText))
    return render_template('affine_reccurent.html', text1 =inputText)
############################################################################


################################
@app.route('/AES')
def aes():
    return render_template("AES.html")


@app.route('/AES_encryption', methods=['POST'])
def aes_encryption():
    inputText = request.form['text']
    input_key = request.form['key']
    while(len(inputText)!=16):
        inputText += ' '
    f = open('text', 'w')
    for i in inputText:
        f.write(i)
    f.close()

    input_path = os.path.abspath('text')
    with open(input_path, 'r') as f:
        data = f.read()
        data = data.encode()

    crypted_data = []
    temp = []
    for byte in data:
        temp.append(byte)
        if len(temp) == 16:
            crypted_part = aes128.encrypt(temp, input_key)
            crypted_data.extend(crypted_part)
            del temp[:]
    else:
        if 0 < len(temp) < 16:
            empty_spaces = 16 - len(temp)
            for i in range(empty_spaces - 1):
                temp.append(0)
            temp.append(1)
            crypted_part = aes128.encrypt(temp, input_key)
            crypted_data.extend(crypted_part)
    msg = bytes(crypted_data)
    msg = base64.b64encode(msg).decode('utf-8')
    return render_template('AES.html', text = msg, key = input_key)


@app.route('/AES_decryption', methods=['POST'])
def aes_decryption():
    inputText = request.form['text1']
    input_key = request.form['key1']

    encodedBytes = base64.b64decode(inputText)

    message = aes128.decrypt(encodedBytes, input_key)
    print("message = " + str(message))
    message = bytes(message).decode('utf-8')


    return render_template('AES.html', text1 = message)
############################################################################




if __name__ == "__main__":
    app.run(debug=True)