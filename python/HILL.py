import numpy as np

#Шифр Хилла
def encryption_hill(text_app, key2_text, action_encrypt, key_py, key_py2):
    if (int(len(key_py)) != 9):
        return "Длинна ключа должна быть = 9 и содержать только символы которые есть в алфавите(см. ниже)"
    def input_text(text):
        Flag = False
        while not Flag:
            for i in text:
                Flag = False
                if alphabet.count(i) == 0:
                    text = 'Текст содержит символы которые отсутствуют в алфавите!!!'
                    break
                else:
                    Flag = True
        return text


    def Hill_encryption(text, summ, alphabet, type_cipher, key, key2):
        key0, key1 = key, key2

        if len(text) < 3:
            while (len(text) != 3):
                text += ' '

        while (len(text) % 3 != 0):  # дополняем матрицу пробелами при необходимости чтоб ее можно было перемножить на ключ
            text += ' '  # добавляем в список символ который отвечает за пробел

        for i in text:
            text_index.append(alphabet.index(i))  # берем данные из текста и преобразуем их в цифры
        n = int(len(text_index) / 3)  # цикл для заполнения всего текста

        if type_cipher == '1':
            for z in range(0, n):
                new_matrix = [text_index[0 + z * 3], text_index[1 + z * 3], text_index[
                    2 + z * 3]]  # будем кодировать символы блоками(по 3), поэтому new_matrix удобная для промежуточного хранения данных
                for j in range(0, 3):  # следующими 2 циклами перемножаем матрицы new_matrix(1x3) и key0(3x3)
                    for i in range(0, 3):
                        summ += new_matrix[i] * key[i][j]
                    text_cipher_index.append(summ % len(
                        alphabet))  # число берем по mod29 или 37 и получаем закодированное число который потом переводим в символ
                    summ = 0
            for i in text_cipher_index:
                text_cipher.append(alphabet[i])  # преобразуем закодированные цифры в символы
            new_matrix = []
            return text_cipher
        else:

            for z in range(0, n):
                new_matrix = [text_index[0 + z * 3], text_index[1 + z * 3], text_index[
                    2 + z * 3]]  # будем кодировать символы блоками(по 3), поэтому new_matrix удобная для промежуточного хранения данных
                for j in range(0, 3):  # следующими 2 циклами перемножаем матрицы new_matrix(1x3) и key0(3x3)
                    for i in range(0, 3):
                        summ += new_matrix[i] * key[i][j]
                    text_cipher_index.append(summ % len(
                        alphabet))  # число берем по mod29 или 37 и получаем закодированное число который потом переводим в символ
                    summ = 0
                if z % 2 == 0:
                    if z == 0:
                        key = key1
                    else:
                        key = key1 = (key1.dot(key0)) % len(alphabet)
                else:
                    key = key0 = (key0.dot(key1)) % len(alphabet)

            for i in text_cipher_index:
                text_cipher.append(alphabet[i])  # преобразуем закодированные цифры в символы
            new_matrix = []
            return text_cipher


    def det_key_inverse(det_key, len_alphabet):
        len_m, y, a1 = len_alphabet, 0, 1
        if len_alphabet == 1:
            return 0
        while (det_key > 1):
            ch, m1 = det_key // len_alphabet, len_alphabet
            len_alphabet, det_key, m1 = det_key % len_alphabet, m1, y
            y, a1 = a1 - ch * y, m1
        if a1 < 0:
            a1 += len_m
        return a1


    def Hill_decryption(text_cipher, summ, alphabet, type_cipher, key, key2):
        key0, key1 = key, key2
        text, text_index, text_cipher_index = [], [], []
        summ = 0.0

        for i in text_cipher:
            text_cipher_index.append(alphabet.index(i))  # берем данные из текста и преобразуем их в цифры
        n = int(len(text_cipher_index) / 3)  # цикл для заполнения всего текста

        if type_cipher == '1':
            det_key = np.linalg.det(key0)
            detKeyInverse = det_key_inverse((det_key % len(alphabet)), len(alphabet))
            key_inverse = np.array(key0)
            key_inverse = np.linalg.inv(key_inverse)
            key_inverse = (((key_inverse * det_key) % len(alphabet)) * detKeyInverse) % len(alphabet)

            for z in range(0, n):
                new_matrix = [text_cipher_index[0 + z * 3], text_cipher_index[1 + z * 3], text_cipher_index[
                    2 + z * 3]]  # будем кодировать символы блоками(по 3), поэтому new_matrix удобная для промежуточного хранения данных

                for j in range(0, 3):  # следующими 2 циклами перемножаем матрицы new_matrix(1x3) и key0(3x3)
                    for i in range(0, 3):
                        summ += float(new_matrix[i]) * (key_inverse[i][j])
                    summ = int(summ + 0.1)
                    text_index.append(summ % len(
                        alphabet))  # число берем по mod29 или 37 и получаем закодированное число который потом переводим в символ
                    summ = 0.0

            for i in text_index:
                text.append(alphabet[i])  # преобразуем закодированные цифры в символы
            return text
        else:
            det_key = np.linalg.det(key0)
            detKeyInverse = det_key_inverse((det_key % len(alphabet)), len(alphabet))
            key_inverse = np.array(key0)
            key_inverse = np.linalg.inv(key_inverse)
            key_inverse = (((key_inverse * det_key) % len(alphabet)) * detKeyInverse) % len(alphabet)

            for z in range(0, n):
                new_matrix = [text_cipher_index[0 + z * 3], text_cipher_index[1 + z * 3], text_cipher_index[
                    2 + z * 3]]  # будем кодировать символы блоками(по 3), поэтому new_matrix удобная для промежуточного хранения данных
                for j in range(0, 3):  # следующими 2 циклами перемножаем матрицы new_matrix(1x3) и key0(3x3)
                    for i in range(0, 3):
                        summ += float(new_matrix[i]) * (key_inverse[i][j])
                    summ = int(summ + 0.1)
                    text_index.append(summ % len(
                        alphabet))  # число берем по mod29 или 74 и получаем закодированное число который потом переводим в символ
                    summ = 0.0
                if z % 2 == 0:
                    if z == 0:
                        det_key = np.linalg.det(key1)
                        detKeyInverse = det_key_inverse((det_key % len(alphabet)), len(alphabet))
                        key_inverse = np.array(key1)
                        key_inverse = np.linalg.inv(key_inverse)
                        key_inverse = (((key_inverse * det_key) % len(alphabet)) * detKeyInverse) % len(alphabet)
                    else:
                        key1 = (key1.dot(key0)) % len(alphabet)
                        det_key = np.linalg.det(key1)
                        detKeyInverse = det_key_inverse((det_key % len(alphabet)), len(alphabet))
                        key_inverse = np.array(key1)
                        key_inverse = np.linalg.inv(key_inverse)
                        key_inverse = (((key_inverse * det_key) % len(alphabet)) * detKeyInverse) % len(alphabet)
                else:
                    key0 = (key0.dot(key1)) % len(alphabet)
                    det_key = np.linalg.det(key0)
                    detKeyInverse = det_key_inverse((det_key % len(alphabet)), len(alphabet))
                    key_inverse = np.array(key0)
                    key_inverse = np.linalg.inv(key_inverse)
                    key_inverse = (((key_inverse * det_key) % len(alphabet)) * detKeyInverse) % len(alphabet)

            for i in text_index:
                text.append(alphabet[i])  # преобразуем закодированные цифры в символы
            return text





    alphabet = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i',

                'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r',

                'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', ' ', ',', '-', ':', '"',
                '!', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',

                'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',

                'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',

                'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я', '№', ';', '.', '1', '2', '3', '4', '5', '6',
                '7', '8', '9', '0']
    text, text_index, text_cipher, text_cipher_index, summ = [], [], [], [], 0
    text_return = ''
    if key2_text == None:  # в случае обычного
        type_cipher = '1'
        key2 = 0
    else:
        key2 = np.array([[0, 12, 29], [16, 9, 14], [9, 8, 13]])  # альпинизм
        i = 0
        if (int(len(key_py2)) != 9):
            return "Длинна ключа должна быть = 9 и содержать только символы которые есть в алфавите(см. ниже)"
        for j in key_py2:
            if (i == 0):
                key2[0][0] = alphabet.index(j)
            if (i == 1):
                key2[0][1] = alphabet.index(j)
            if (i == 2):
                key2[0][2] = alphabet.index(j)
            if (i == 3):
                key2[1][0] = alphabet.index(j)
            if (i == 4):
                key2[1][1] = alphabet.index(j)
            if (i == 5):
                key2[1][2] = alphabet.index(j)
            if (i == 6):
                key2[2][0] = alphabet.index(j)
            if (i == 7):
                key2[2][1] = alphabet.index(j)
            if (i == 8):
                key2[2][2] = alphabet.index(j)
            i += 1
        type_cipher = '2'
    key = np.array([[11, 15, 12], [15, 2, 15], [17, 15, 19]])  # коловорот
    i = 0
    for j in key_py:
        if (i == 0):
            key[0][0] = alphabet.index(j)
        if (i == 1):
            key[0][1] = alphabet.index(j)
        if (i == 2):
            key[0][2] = alphabet.index(j)
        if (i == 3):
            key[1][0] = alphabet.index(j)
        if (i == 4):
            key[1][1] = alphabet.index(j)
        if (i == 5):
            key[1][2] = alphabet.index(j)
        if (i == 6):
            key[2][0] = alphabet.index(j)
        if (i == 7):
            key[2][1] = alphabet.index(j)
        if (i == 8):
            key[2][2] = alphabet.index(j)
        i+=1
    text = input_text(text_app)
    if(text == 'Текст содержит символы которые отсутствуют в алфавите!!!'):
        return text
    if action_encrypt == 'encrypt':
        text_return = Hill_encryption(text_app, summ, alphabet, type_cipher, key, key2)
    if action_encrypt == 'decrypt':
        text_return = Hill_decryption(text_app, summ, alphabet, type_cipher, key, key2)
    return text_return