#Афинный шифр
def encryption_affin(text_app, a, b, a1, b1, action_encrypt):

    def input_text(text):
        Flag = False
        while not Flag:
            for i in text:
                Flag = False
                if alphabet.count(i) == 0:
                    text = 'Текст содержит символы которые отсутствуют в алфавите!!!'
                    break
                else:
                    Flag = True
        return text

    def input_a(a):  # Корректный ввод а
        Flag = False
        while not Flag:
            if a % 2 != 0 and a <= 137 and a > 0:
                Flag = True
            else:
                text = 'Ошибка: а не являается взаимнопростым числом относительно величины  алфавита или число введено некорректно'
        return a

    def a_1(a, m):
        len_m = m
        y = 0
        a1 = 1

        if m == 1:  # Если длинна алфавита = 1 то обратный элемент = 0, это можно посчитать вручную
            return 0

        while (a > 1):
            # находим частное
            ch = a // m
            m1 = m

            # Алгоритм Евклида
            m = a % m
            a = m1
            m1 = y

            # Обновляем переменные
            y = a1 - ch * y
            a1 = m1

        if a1 < 0:
            a1 += len_m
        return a1


    def Affin_encryption(text, alphabet, type_cipher, a, b, a1, b1):
        text_index, cipher_index = [], []
        text_cipher = []
        a_list = [None] * len(alphabet)
        b_list = [None] * len(alphabet)

        a_list[0] = a
        b_list[0] = b
        a_list[1] = a1
        b_list[1] = b1

        for i in range(2, len(alphabet)):
            a_list[i] = (a_list[i - 1] * a_list[i - 2]) % len(alphabet)
            b_list[i] = (b_list[i - 1] * b_list[i - 2]) % len(alphabet)


        if type_cipher == '1':
            for i in text:  # Цикл
                text_index.append(alphabet.index(i))  # Формула перевода текста в цифры(номера этих букв в алфавите)
            for i in range(len(text_index)):
                cipher_index.append((a * text_index[i] + b) % len(alphabet))
            for i in range(len(cipher_index)):
                text_cipher.append(alphabet[cipher_index[i]])  # Перевод зашифрованных цифр в зашифрованный текст
            return text_cipher
        else:
            for i in text:
                text_index.append(alphabet.index(i))
            for i in range(len(text_index)):
                cipher_index.append((a_list[i] * text_index[i] + b_list[i]) % len(alphabet))  # Формула зашифровки, и получение зашифрованных цифр
            for i in range(len(cipher_index)):
                text_cipher.append(alphabet[cipher_index[i]])  # Получение зашифрованного текста из зашифрованных цифр
            return text_cipher



    def Affin_decryption(text_app, alphabet, type_cipher, a, b, a1, b1):
        text_index, text_cipher_index, text = [], [], []
        print("text_app = " + text_app)
        text_cipher = text_app
        a_list = [None] * len(alphabet)
        b_list = [None] * len(alphabet)
        a1_list = [None] * len(alphabet)

        a_list[0] = a
        b_list[0] = b
        a_list[1] = a1
        b_list[1] = b1

        for i in range(2, len(alphabet)):
            a_list[i] = (a_list[i - 1] * a_list[i - 2]) % len(alphabet)
            b_list[i] = (b_list[i - 1] * b_list[i - 2]) % len(alphabet)


        if type_cipher == '1':
            # Расшифрование начинается отсюда
            a1 = a_1(a, len(alphabet))  # Обращаемся к функции для получения а1
            for i in text_cipher:
                text_cipher_index.append(alphabet.index(i))  # Перевод шифра в цифры(номера этих букв в алфавите)
            for i in range(len(text_cipher_index)):
                text_index.append(a1 * (text_cipher_index[i] + len(alphabet) - b) % len(alphabet))
            for i in range(len(text_index)):
                text.append(alphabet[text_index[i]])  # Перевод  цифр в расшифрованный текст
            return text

        else:
            for i in text_cipher:
                text_cipher_index.append(alphabet.index(i))  # Получение цифр из текста
            for i in range(len(text_cipher_index)):
                a1_list[i] = a_1(a_list[i], len(alphabet))
                text_index.append(
                    a1_list[i] * (text_cipher_index[i] + len(alphabet) - b_list[i]) % len(alphabet))  # Формула расшифровки
            for i in range(len(text_index)):
                text.append(alphabet[text_index[i]])  # .append это присоединение того что в скобках к массиву
            return text


    alphabet = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i',

                'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r',

                'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', ' ', ',', '-', ':', '"',
                '!', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',

                'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',

                'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',

                'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я', '№', ';', '.', '1', '2', '3', '4', '5', '6',
                '7', '8', '9', '0']


    text_return = ''
    if a1 == None:  # в случае обычного
        type_cipher = '1'
        a1, b1 = 0, 0
    else:
        type_cipher = '2'
    text = input_text(text_app)
    if(text == 'Текст содержит символы которые отсутствуют в алфавите!!!'):
        return text
    if action_encrypt == 'encrypt':
        text_return = Affin_encryption(text, alphabet, type_cipher, a, b, a1, b1)
    if action_encrypt == 'decrypt':
        print(text)
        text_return = Affin_decryption(text, alphabet, type_cipher, a, b, a1, b1)
        print(text_return)
    return text_return


