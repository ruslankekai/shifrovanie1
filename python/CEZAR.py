def input_text(text):
    Flag = False
    while not Flag:
        for i in text:
            Flag = False
            if alphabet.count(i) == 0:
                text = 'Текст содержит символы которые отсутствуют в алфавите!!!'
                break
            else:
                Flag = True
    return text

def cezar_crypt(text, shift, action):
    text_encode, text_index, text_encode, text_encode_index  = [], [], [], []
    text = input_text(text)
    if (text == 'Текст содержит символы которые отсутствуют в алфавите!!!'):
        return text

    if action == 'encrypt':
        for i in text:  # Цикл
            if alphabet.index(i)+shift >= 137:
                text_encode.append(alphabet[alphabet.index(i)+shift-137])
            else:
                text_encode.append(alphabet[alphabet.index(i)+shift])
    else:
        for i in text:  # Цикл
            if alphabet.index(i) - shift < 0:
                text_encode.append(alphabet[alphabet.index(i)-shift+137])
            else:
                text_encode.append(alphabet[alphabet.index(i)-shift])
    return text_encode




alphabet = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i',

            'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r',

            'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', ' ', ',', '-', ':', '"',
            '!', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',

            'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',

            'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',

            'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я', '№', ';', '.', '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0']
