import unittest
from AFFINE_for_test import encryption_affin, programm

#python -m unittest test_affine.py

#git commit -m "текст"
#git push heroku master


class TestAFFINE(unittest.TestCase):
    def setUp(self):
        self.test_encryption_affin = encryption_affin()

    def test_input_a(self):
        self.assertEqual(self.test_encryption_affin.input_a(3), 3)
        self.assertEqual(self.test_encryption_affin.input_a(-1), False)
        self.assertEqual(self.test_encryption_affin.input_a(138), False)
        self.assertEqual(self.test_encryption_affin.input_a(4), False)


    def test_input_text(self):
        self.assertEqual(self.test_encryption_affin.input_text('*'), 'Текст содержит символы которые отсутствуют в алфавите!!!')


    def test_Affin_encryption(self):
        alphabet = ['A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i',

                    'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r',

                    'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z', ' ', ',', '-', ':',
                    '"',
                    '!', 'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',

                    'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',

                    'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',

                    'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я', '№', ';', '.', '1', '2', '3', '4', '5',
                    '6',
                    '7', '8', '9', '0']

        self.assertEqual(programm(self, 'Hello, меня зовут Саша!!!', 7, 15, None, None, 'encrypt', alphabet), 'ъЙttкЪцВнИ-ц3Пwleц;iрibbb')
        self.assertEqual(programm(self, 'ъЙttкЪцВнИ-ц3Пwleц;iрibbb', 7, 15, None, None, 'decrypt', alphabet), 'Hello, меня зовут Саша!!!')

        self.assertEqual(programm(self, 'Hello, меня зовут Саша!!!', 7, 15, 13, 22, 'encrypt', alphabet),'ъBСЛВyЖЬмхцTпЗЛvюVp7kiПhG')
        self.assertEqual(programm(self, 'ъBСЛВyЖЬмхцTпЗЛvюVp7kiПhG', 7, 15, 13, 22, 'decrypt', alphabet),'Hello, меня зовут Саша!!!')

if __name__ == "__main__":
  unittest.main()





